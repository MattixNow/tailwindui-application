/*
** TailwindCSS Configuration File
**
** Docs: https://tailwindcss.com/docs/configuration
** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
*/
module.exports = {
  theme: {
    extend: {
      colors: {
        primary: {
          light: '#D11B1B',
          default: '#a31616',
          dark: '#610D0D'
        },
        secondary: {
          lighter: '#363636',
          light: '#1E1E1E',
          default: '#161616',
        }
      }
    }
  },
  variants: {},
  plugins: []
}
